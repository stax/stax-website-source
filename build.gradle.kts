import org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi

plugins {
    kotlin("multiplatform") version "1.9.22"
    id("com.github.ben-manes.versions") version "0.50.0"
    application
}

group = "io.github.rebokdev"
version = 1

repositories {
    mavenCentral()
}

kotlin {
    jvm {
        @OptIn(ExperimentalKotlinGradlePluginApi::class)
        mainRun {
            mainClass = "MainKt"
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-html:0.10.1")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.7.3")
                implementation("org.jetbrains.kotlin-wrappers:kotlin-css:1.0.0-pre.685")
                implementation("com.soywiz.korlibs.korio:korio:4.0.10")
            }
        }
    }
}