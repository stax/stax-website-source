import kotlinx.coroutines.runBlocking
import kotlinx.html.HTML
import kotlinx.html.html
import kotlinx.html.stream.createHTML

class Page(
    val name: String,
    private val body: suspend HTML.() -> Unit
) {
    fun generate(): String = createHTML().html {
        runBlocking {
            body()
        }
    }
}