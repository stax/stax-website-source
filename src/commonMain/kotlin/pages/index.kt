package pages

import Page
import kotlinx.css.*
import kotlinx.css.properties.AspectRatio
import kotlinx.css.properties.TextDecoration
import kotlinx.css.properties.Time
import kotlinx.html.*
import shared.Fonts
import shared.mainCss

val index = Page(
    name = "index",
    body = {
        head {
            style {
                unsafe {
                    +mainCss
                }
            }

            unsafe {
                + Fonts.NunitoSans1000
                + Fonts.documentIcon
            }

            style {
                unsafe {
                    +CssBuilder().apply {
                        rule("#Enormous-stax-text") {
                            fontSize = 10.em
                            fontFamily = "Nunito Sans"
                            userSelect = UserSelect.none
                        }

                        rule("#Short-stax-description-text") {
                            fontSize = 1.em
                            fontFamily = "Nunito Sans"
                            userSelect = UserSelect.none
                            marginTop = (-8).em
                        }

                        rule("#main-div") {
                            display = Display.flex
                            flexDirection = FlexDirection.column
                            alignItems = Align.center
                            justifyItems = JustifyItems.center
                        }

                        rule("#Get_started_button") {
                            color = Color.white
                            backgroundColor = Color.green
                            padding = Padding(1.em)
                            borderRadius = 5.em
                            fontFamily = "Nunito Sans"
                            transitionDuration = Time("0.1s")
                            border = Border(.05.em, BorderStyle.solid, Color.transparent)
                            textDecoration = TextDecoration.none
                            userSelect = UserSelect.none

                            hover {
                                backgroundColor = Color.darkGreen
                                border = Border(.05.em, BorderStyle.solid, Color.lightGray)
                            }
                        }

                        rule("#Buttons_box") {
                            display = Display.flex
                            flexDirection = FlexDirection.row
                            alignItems = Align.center
                            gap = .2.em
                        }

                        rule("#Docs_button") {
                            padding = Padding(1.em)
                            borderRadius = 100.pct
                            backgroundColor = Color.gray
                            fontFamily = "Nunito Sans"
                            aspectRatio = AspectRatio("1/1")
                            color = Color.white
                            transitionDelay = Time("0.1s")
                            border = Border(.05.em, BorderStyle.solid, Color.gray)
                            userSelect = UserSelect.none

                            hover {
                                //color = Color.gray
                                border = Border(.05.em, BorderStyle.solid, Color.lightGray)
                            }
                        }
                    }.toString()
                }
            }
        }

        body {
            div("center") {
                id = "main-div"

                h1("text") {
                    id = "Enormous-stax-text"
                    +"Stax"
                }

                h2("text") {
                    id = "Short-stax-description-text"
                    +"A future centric spreadsheet format made for devs"
                }

                div {
                    id = "Buttons_box"

                    a {
                        id = "Get_started_button"
                        href = "https://codeberg.org/stax"

                        +"get started"
                    }

                    a {
                        id = "Docs_button"
                        href = "https://codeberg.org/stax/stax-docs/wiki"

                        span {
                            classes = setOf("material-symbols-outlined")

                            + "description"
                        }
                    }
                }
            }
        }
    }
)
