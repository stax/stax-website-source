import korlibs.io.file.VfsFile

suspend fun savePage(
    page: Page,
    inDirectory: VfsFile
) {
    val pageResult = page.generate()
    val targetFile = inDirectory["${page.name}.html"]
    targetFile.writeString(pageResult)
}