package shared

import kotlinx.css.*
import kotlinx.css.properties.transform
import kotlinx.css.properties.translate

val mainCss = CssBuilder().apply {
    body {
        backgroundColor = Color.black
    }

    rule(".center") {
        position = Position.absolute
        left = 50.pct
        top = 50.pct

        transform {
            translate(
                (-50).pct,
                (-50).pct
            )
        }
    }

    rule(".max-width") {
        width = 100.pct
    }

    rule(".text") {
        color = Color.white
    }
}.toString()